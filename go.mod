module tweets-collector

go 1.14

require (
	github.com/DataDog/zstd v1.4.0 // indirect
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
	github.com/prometheus/client_golang v1.7.1
	github.com/segmentio/kafka-go v0.3.6
	go.uber.org/zap v1.15.0
)
