package env

import (
	"log"
	"os"
)

func RetriveEnvVarible(ENV string) string {
	value, exists := os.LookupEnv(ENV)
	if !exists {
		log.Fatalf("Environment Variable %s is missing\n", ENV)
		os.Exit(1)
	}
	return value
}
