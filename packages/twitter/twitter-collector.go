package twitter

import (
	"strings"
	"time"
	"tweets-collector/packages/env"
	"tweets-collector/packages/logger"
	"tweets-collector/packages/metrics"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

var log = logger.GetLogger()

func TwitterCollector(
	consumerKey string,
	consumerSecret string,
	tokenKey string,
	tokenSecret string,
	tweetsChannel chan<- *twitter.Tweet,
	cycleTime int,
	collectTrends bool,
) {
	cycleTimeTicker := time.NewTicker(time.Duration(cycleTime) * time.Second)

	streamCheckChannel, stream := collectTweets(consumerKey, consumerSecret, tokenKey, tokenSecret, tweetsChannel, collectTrends)

	if collectTrends {
		for {
			select {
			case _, ok := <-streamCheckChannel:
				if !ok {
					streamCheckChannel, stream = collectTweets(consumerKey, consumerSecret, tokenKey, tokenSecret, tweetsChannel, collectTrends)
				}
			case <-cycleTimeTicker.C:
				log.Info("Cycle time ticked")
				stream.Stop()
				close(streamCheckChannel)
				streamCheckChannel, stream = collectTweets(consumerKey, consumerSecret, tokenKey, tokenSecret, tweetsChannel, collectTrends)
			}
		}
	} else {
		for {
			select {
			case _, ok := <-streamCheckChannel:
				if !ok {
					streamCheckChannel, stream = collectTweets(consumerKey, consumerSecret, tokenKey, tokenSecret, tweetsChannel, collectTrends)
				}
			}
		}
	}
}

func collectTweets(
	consumerKey string,
	consumerSecret string,
	tokenKey string,
	tokenSecret string,
	tweetsChannel chan<- *twitter.Tweet,
	collectTrends bool,
) (chan int, *twitter.Stream) {

	streamCheckChannel := make(chan int)

	config := oauth1.NewConfig(consumerKey, consumerSecret)
	token := oauth1.NewToken(tokenKey, tokenSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	streamTrack := getStreamFilter(client, collectTrends)
	log.Infow("Stream filter", "filter", streamTrack)

	demux := twitter.NewSwitchDemux()

	demux.Tweet = func(tweet *twitter.Tweet) {
		metrics.CollectedTweets.Inc()
		tweetsChannel <- tweet
	}

	demux.StreamLimit = func(limit *twitter.StreamLimit) {
		log.Warnw("Number of undelivered matches because rate limit", "count", limit.Track)
		metrics.UndeliveredMatches.Add(float64(limit.Track))
	}

	demux.Warning = func(warning *twitter.StallWarning) {
		log.Warnw("Warning from twitter stream", "message", warning)
	}

	params := &twitter.StreamFilterParams{
		Track:         streamTrack,
		StallWarnings: twitter.Bool(true),
		Language:      []string{"ar"},
	}

	stream, err := client.Streams.Filter(params)

	if err != nil {
		log.Errorw("Error while opening stream", "message", err)
		stream.Stop()
		close(streamCheckChannel)
	}

	demux.StreamDisconnect = func(disconnect *twitter.StreamDisconnect) {
		log.Errorw("Stream disconnected from twitter", "message", disconnect)
		stream.Stop()
		close(streamCheckChannel)
	}

	// Receive messages until stopped or stream quits
	go demux.HandleChan(stream.Messages)

	return streamCheckChannel, stream
}

func getStreamFilter(client *twitter.Client, collectTrends bool) []string {

	if collectTrends {
		streamTrack := make([]string, 0)
		list, response, err := client.Trends.Place(23424938, nil)

		if err != nil {
			log.Errorw("Error while collecting trends", "response", response, "error", err)
		}

		for _, element := range list[0].Trends {
			streamTrack = append(streamTrack, element.Name)
		}
		return streamTrack

	}
	streamTrack := strings.Split(env.RetriveEnvVarible("COLLECTION_LIST"), ",")
	return streamTrack

}
