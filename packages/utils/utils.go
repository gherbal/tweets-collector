package utils

import (
	"os"
	"strconv"
	"tweets-collector/packages/logger"
)

var log = logger.GetLogger()

func ParseInt(stringInt string) int {
	integer, err := strconv.Atoi(stringInt)
	if err != nil {
		log.Fatalf("Couldn't parse %s as an int\n", stringInt)
		os.Exit(1)
	}
	return integer
}
