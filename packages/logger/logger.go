package logger

import (
	"tweets-collector/packages/env"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logLevel string = env.RetriveEnvVarible("LOG_LEVEL")

func GetLogger() *zap.SugaredLogger {

	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder // overwrite Epoch time encoding
	config := zap.NewProductionConfig()
	config.EncoderConfig = encoderConfig
	config.Level.SetLevel(getZapLevel(logLevel))
	logger, _ := config.Build()

	defer logger.Sync()
	return logger.Sugar()
}

func getZapLevel(level string) zapcore.Level {
	switch level {
	case "INFO":
		return zapcore.InfoLevel
	case "WARN":
		return zapcore.WarnLevel
	case "DEBUG":
		return zapcore.DebugLevel
	case "ERROR":
		return zapcore.ErrorLevel
	case "FATAL":
		return zapcore.FatalLevel
	default:
		return zapcore.InfoLevel
	}
}
