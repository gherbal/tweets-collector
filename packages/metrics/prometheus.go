package metrics

import (
	"net/http"
	"tweets-collector/packages/logger"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var log = logger.GetLogger()

var (
	CollectedTweets = promauto.NewCounter(
		prometheus.CounterOpts{
			Namespace: "tweets_collector",
			Name:      "collected_tweets_count",
			Help:      "The total number of collected tweets",
		})
	PublishedTweets = promauto.NewCounter(
		prometheus.CounterOpts{
			Namespace: "tweets_collector",
			Name:      "published_tweets_count",
			Help:      "The total number of published tweets",
		})
	UndeliveredMatches = promauto.NewCounter(
		prometheus.CounterOpts{
			Namespace: "tweets_collector",
			Name:      "twitter_api_undelivered_matches_count",
			Help:      "The total number of undelivered matches from twitter API",
		})
)

func StartPrometheusServer() {
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
	log.Info("Started Prometheus server")
}
