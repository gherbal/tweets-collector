package kafka

import (
	"context"
	"encoding/json"
	"os"
	"tweets-collector/packages/logger"
	"tweets-collector/packages/metrics"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/segmentio/kafka-go"
)

var log = logger.GetLogger()

// PublishTweets is function that receive tweets channel and publish those tweets to Kafka
func PublishTweets(kafkaBrokers string, kafkaTopic string, tweetsChannel <-chan *twitter.Tweet) {
	writer := kafka.NewWriter(
		kafka.WriterConfig{
			Brokers: []string{kafkaBrokers},
			Topic:   kafkaTopic,
		})

	buffer := make([]kafka.Message, 100)
	counter := 0

	for tweet := range tweetsChannel {
		tweetJSON, err := json.Marshal(tweet)

		if err != nil {
			log.Errorw("Can't encode tweet to JSON", "id", tweet.IDStr)
		} else {
			message := kafka.Message{Value: tweetJSON}
			buffer[counter] = message
			counter++
		}

		if counter == 100 {
			writeError := writer.WriteMessages(context.Background(), buffer...)
			if writeError != nil {
				log.Errorw("Error writing messages to Kafka", "error", writeError)
			} else {
				metrics.PublishedTweets.Add(100)
			}
			buffer = make([]kafka.Message, 100)
			counter = 0
		}
	}

	err := writer.Close()

	if err != nil {
		log.Errorw("Error while closing Kafka writer", "error", err)
	}

	log.Info("Closed Kafka writer")
	os.Exit(1)
}
