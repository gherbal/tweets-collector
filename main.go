package main

import (
	"strconv"
	"tweets-collector/packages/env"
	"tweets-collector/packages/kafka"
	"tweets-collector/packages/metrics"
	"tweets-collector/packages/twitter"
	"tweets-collector/packages/utils"

	goTwitter "github.com/dghubble/go-twitter/twitter"
)

var (
	consumerKey    = env.RetriveEnvVarible("CONSUMER_KEY")
	consumerSecret = env.RetriveEnvVarible("CONSUMER_SECRET")
	tokenKey       = env.RetriveEnvVarible("ACCESS_TOKEN_KEY")
	tokenSecret    = env.RetriveEnvVarible("ACCESS_TOKEN_SECRET")
	kafkaBrokers   = env.RetriveEnvVarible("KAFKA_BROKERS")
	kafkaTopic     = env.RetriveEnvVarible("KAFKA_TOPIC")
	cycleTime      = utils.ParseInt(env.RetriveEnvVarible("CYCLE_TIME"))
	collectTrends  = env.RetriveEnvVarible("COLLECT_TRENDS")
)

func main() {
	go metrics.StartPrometheusServer()

	tweetsChannel := make(chan *goTwitter.Tweet, 1000)
	collectTrendsBool, _ := strconv.ParseBool(collectTrends)
	go kafka.PublishTweets(kafkaBrokers, kafkaTopic, tweetsChannel)
	twitter.TwitterCollector(consumerKey, consumerSecret, tokenKey, tokenSecret, tweetsChannel, cycleTime, collectTrendsBool)
}
