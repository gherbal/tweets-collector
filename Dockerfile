FROM golang:1.14.6-alpine3.12

WORKDIR /go/src/tweets-collector

COPY go.* ./

RUN go mod download

COPY main.go ./

COPY packages ./packages/

RUN go build

CMD ["./tweets-collector"]
